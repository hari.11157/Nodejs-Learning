var events = require('events');
var util = require('util');
var eventEmitter = new events.EventEmitter();


/* events sample code */
var ringbell = function(){
console.log("bell is ringing, i think some customer have arrived")
}
eventEmitter.on('dooropen',ringbell);
eventEmitter.on('dooropen',ringbell);
eventEmitter.emit('dooropen');
eventEmitter.emit('dooropen');
var welcome = function(){
    console.log("hi, welcome to our shop");
}
eventEmitter.on('customer enters',welcome);
eventEmitter.emit('customer enters');

/*once event sample code */
var mynewevent = function(){
    console.log("customer buyed some things");
}
eventEmitter.once('customer buyed things',mynewevent);

eventEmitter.emit('customer buyed things');
eventEmitter.emit('customer buyed things');
eventEmitter.emit('customer buyed things');
eventEmitter.emit('customer buyed things');


/*inheritance sample code */
/*but inheritance is deprecated in nodejs, we go for extends*/
var Students = function(name){
    this.name = name;
}
util.inherits(Students,events.EventEmitter);

var hari = new Students('hari');
hari.on('scored',function(marks){
    console.log(hari.name + " scored " + marks);
});
hari.emit('scored' , 90);

/* error events */
// class myEmitter extends events{}

var emitter = new myEmitter();
// myEmitter.emit('error',new error('whoops'));

// process.on('uncaught_exception',function(err){
//     console.log("there was an error");
// });

/*event count*/
console.log(events.listenerCount(eventEmitter, 'dooropen'));

var callback = function(stream) {
    console.log('someone connected!');
};

eventEmitter.on('connection', callback);
eventEmitter.removeListener('connection', callback);