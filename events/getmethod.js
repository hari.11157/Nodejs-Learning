var express = require('express');
var app = express();
var path = require('path');
var fs = require('fs');

app.get('/',function(req,res){
    res.sendFile('sample.html', {root: path.join(__dirname, '../express')});
});

app.get(/^(.+)$/, function (req,res) {
    try {
        if (fs.statSync(path.join(__dirname, '../express', req.params[0] + '.html')).isFile()) {
            res.sendFile(req.params[0] + '.html', {root: path.join(__dirname, '../express')});
        }
    }
        catch(err){
            res.sendFile('404.html', {root: path.join(__dirname, '../express')});
        }

})

app.listen(1337,function () {
    console.log("app listening on port:1337")
})