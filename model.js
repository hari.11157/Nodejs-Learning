var mysql = require('mysql');
var express = require('express');
var validate = require('express-validation');
var passwordValidator = require('password-validator');
var bodyParser = require('body-parser');
var passwordHash = require('password-hash');
var app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var my_module = require('./model1');

app.post('/records', function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    my_module.createdevice(req,res);
});

app.listen(1234,function(){
    console.log("server listening on port:1234");
});