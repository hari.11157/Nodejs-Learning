// function fn1(){
// 	console.log("this o/p is from first function");
// }
//
// function fn2(){
// 	console.log("this o/p is from second function");
// }
//
// function fn3(){
// 	console.log("this o/p is from third function");
// }
//
// /* now export the functions which you are required*/
//
// module.exports.func1 = fn1;
// module.exports.func2 = fn2;

/*exporting in other way*/
module.exports = {
	function123 : function(){
		console.log("this o/p is from new way of export");
	},
	function1234 : function(){
		console.log("this o/p is from sec fn of new way of export");
	},
	functionrect : function (a,b) {
		var area = a * b;
        console.log(area);
    }

}
