var mysql = require('mysql');
var express = require('express');
var validate = require('express-validation');
var passwordValidator = require('password-validator');
var bodyParser = require('body-parser');
var passwordHash = require('password-hash');
var app = express();

var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "mydb"
});

module.exports = {
    createdevice : function(req,res){
        var sql = "create table if not exists sampletable(id int(10) primary key auto_increment,uid int(15) not null unique,imei varchar(25) not null unique,androidId varchar(25) not null unique);";
        con.query(sql,function(err,res){
            if(err){
                console.log("error occured " +err);
            }
            if(res){
                console.log(res);
            }
        });
        var query = con.query('insert into sampletable set ?', req.body, function (err, result) {
            if (err) {
                res.status(400).send("error"+err);
                console.log(err);
            }
            if (result) {
                res.status(200).send(result);
                console.log(result);
            }
        });
    }
};

