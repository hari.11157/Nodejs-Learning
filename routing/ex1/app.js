var mysql = require('mysql');
var express = require('express');
var router  = express.Router();
var validate = require('express-validation');
var passwordValidator = require('password-validator');
var jade = require('jade');
var app = express();

app.set('view engine', 'jade');


var index = require('./index');
var example = require('./example1');

app.use('/',index);
app.use('/all',example);



app.listen(1234,function(){
    console.log("server listening on port:1234");
});