var express = require('express');
var app = express();

app.get('/', function (req, res) {
    res.send('hello world');
});


app.post('/', function (req, res) {
    res.send('POST request to the homepage');
});

app.all('/example/b', function (req, res, next) {
        console.log('Accessing the secret section ...');
    next()
    // pass control to the next handler
},
    function (req, res) {
        res.send('Hello from B!');
    });


app.listen(1234,function(){
    console.log("server listening on port:1234");
});

