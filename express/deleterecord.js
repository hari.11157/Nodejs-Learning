var mysql = require('mysql');
var express = require('express');
var validate = require('express-validation');
var passwordValidator = require('password-validator');
var app = express();

var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "mydb"
});

con.connect(function(err){
    if (err){
        console.log("oops! there is an error");
    }
    else{
        console.log("connected");
    }
});

app.delete('/all/:id',function (req,res) {
    var id= req.params.id;
    var sqldisplay1 = "select id from stud_details where id="+id+";";
    con.query(sqldisplay1, function (err,result) {
        var mylength = result.length;
        if(mylength === 0){
            res.send(400,"oops, no record exists");
        }
        if(mylength !== 0){
            var sqldisplay = "DELETE FROM stud_details where id="+id+";";
            con.query(sqldisplay, function (err, result) {
                if(err){
                    res.send(400,"sorry, no record exists with that id");
                }
                if(result){
                    res.send(200,"record deleted successfully");
                }
            });
        }
    });
});

app.listen(1234,function(){
    console.log("server listening on port:1234");
});