var mysql = require('mysql');
var express = require('express');
var validate = require('express-validation');
var passwordValidator = require('password-validator');
var passwordHash = require('password-hash');
var app = express();

var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "mydb"
});

con.connect(function(err){
    if (err){
        console.log("oops! there is an error");
    }
    else{
        console.log("connected");
    }
});

var post = {
    firstname : "ram",
    lastname : "hari",
    email : "hareram41219@gmail.com",
    mobile : 9676290419,
    password : "hareram"
};

var schema = new passwordValidator();
var schema1 = new passwordValidator();
schema
    .is().min(6)
    .is().max(10)
    ;

schema1
    .is().min(1)
;

if(schema.validate(post.password) === false){
    console.log("password must be between 6 to 10 characters");
}

if(schema1.validate(post.firstname) === false){
    console.log("firstname cannot be empty");
}

if(schema.validate(post.password) && schema1.validate(post.firstname)){
    var hashedPassword = passwordHash.generate(post.password);
    post.password = hashedPassword;
    console.log(post.password);
    var query = con.query('insert into stud_details set ?', post, function (err,result) {
        if(err){
            console.log("error occured " +err);
        }
        if(result){
            console.log(result);
        }
    });
}

app.listen(1233,function(){
    console.log("server listening on port:1233");
});