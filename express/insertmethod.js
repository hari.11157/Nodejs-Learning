    var mysql = require('mysql');
    var express = require('express');
    var validate = require('express-validation');
    var passwordValidator = require('password-validator');
    var bodyParser = require('body-parser');
    var passwordHash = require('password-hash');
    var app = express();

    var con = mysql.createConnection({
        host: "localhost",
        user: "root",
        password: "",
        database: "mydb"
    });

    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());

    app.post('/api/records', function (req, res, next) {
    res.setHeader('Content-Type', 'application/json');
    var pwdLength=req.body.password.length;
    var fnameLength=req.body.firstname.length;
    if(fnameLength === 0){
            res.status(200).send("fname cannot be empty");
    }

    else if(pwdLength < 6 || pwdLength > 10){
         res.status(400).send("password s/b in b/n 6 to 10 chars");
    }

    else{
        var hashedPassword = passwordHash.generate(req.body.password);
        req.body.password = hashedPassword;
             var query = con.query('insert into stud_details set ?', req.body, function (err, result) {
             if (err) {

                 res.status(400).send("error"+err);
             }
             if (result) {

                 res.status(200).send(result);
             }

         });
     }
    });

    app.listen(1234,function(){
        console.log("server listening on port:1234");
    });