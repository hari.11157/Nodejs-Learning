var express = require('express');
var app = express();

app.get('/hi',function(req,res){
    res.send("hi i'm from express, and its cool");
});

app.use('/txtfiles',express.static(__dirname + '../modules/myfile'))

app.listen(1337, function(){
    console.log("app listening on 1337");
});

