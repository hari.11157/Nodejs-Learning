var mysql = require('mysql');
var express = require('express');
var app = express();


var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "mydb"
});

con.connect(function(err){
    if (err){
        console.log("oops! there is an error");
    }
    else{
        console.log("connected");
    }
});

app.get('/create_table',function(req,res){
    var sql = "create table stud_details(id int,firstname varchar(255) not null,lastname varchar(255),email varchar(255) primary key,mobile bigint(10) unique,password varchar(10), constraint chk_age check (password >=6 and password <=10));";
    con.query(sql,function(err,result){
        if(err){
            console.log("error occured " +err);
        }
        if(result){
            console.log(result);
        }
        res.send("table is created");
    });
});

app.listen(1337,function(){
    console.log("server listening on port:1337");
})